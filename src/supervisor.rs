use std::{collections::HashMap, time::Duration};

use crate::{def, os};
use rustix::process as pr;

macro_rules! warnic {
    ($($ar:expr),+) => {
        log::warn!($($ar),+);
        #[cfg(debug_assertions)]
        panic!($($ar),+);
    };
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ProcState {
    Running,
    Stopped,
    Exiting,
    Zombie,
}

impl ProcState {
    fn is_terminated(self) -> bool {
        self == ProcState::Exiting || self == ProcState::Zombie
    }
}

#[derive(Debug)]
pub struct PidState {
    pub pid: pr::Pid,
    pub state: ProcState,
}

#[derive(PartialEq, Clone, Copy, Debug)]
enum WaitStage {
    Initial,
    Soft,
    Hard,
}

#[derive(PartialEq)]
enum ServiceState {
    Fresh,
    WaitingForStartTimeout(os::Timeout),
    FailedToStart,
    Running(pr::Pid, os::GroupId),
    WaitingForGroupShutdown(os::GroupId, WaitStage, os::Timeout),
    Done,
    Removed,
}

impl ServiceState {
    fn log(&self, name: &str) {
        match self {
            ServiceState::Fresh => log::debug!("service {} is fresh", name),
            ServiceState::Running(svcpid, grp) => log::debug!(
                "service {} is running as group {:?} with main pid {:?}",
                name,
                grp,
                svcpid
            ),
            ServiceState::WaitingForGroupShutdown(grp, stg, _) => log::debug!(
                "service {} is waiting for group shutdown as group {:?}, {:?}",
                name,
                grp,
                stg
            ),
            ServiceState::WaitingForStartTimeout(_) => {
                log::debug!("service {} is waiting for start timeout", name)
            }
            ServiceState::Done => log::debug!("service {} is done", name),
            ServiceState::FailedToStart => log::debug!("service {} is failed", name),
            ServiceState::Removed => log::debug!("service {} is removed", name),
        }
    }
}

struct Service {
    state: ServiceState,
    defn: def::Service,
    removing: bool,
    start_failure_count: u32,
}

impl Service {
    fn new(defn: def::Service) -> Self {
        Service {
            defn,
            // TODO: non-instant starts (activation etc.)
            state: ServiceState::Fresh,
            removing: false,
            start_failure_count: 0,
        }
    }

    fn start(&mut self, name: &str, ctx: &mut os::Ctx) {
        match os::launch(self.defn.command.clone()) {
            Ok((pid, grp)) => {
                log::debug!("started service {} as pid {:?}", name, pid);
                self.state = ServiceState::Running(pid, grp);
            }
            Err(e) => {
                let max_fails = self.defn.max_start_failures.unwrap_or(5);
                log::warn!(
                    "service {} failed to start (attempt #{}/{}): {:?}",
                    name,
                    self.start_failure_count,
                    max_fails,
                    e
                );
                if self.start_failure_count > max_fails {
                    self.state = ServiceState::FailedToStart;
                } else {
                    let d = self
                        .defn
                        .failure_restart_delay_ms
                        .or(self.defn.restart_delay_ms)
                        .unwrap_or(2000);
                    self.state = ServiceState::WaitingForStartTimeout(
                        ctx.new_timeout(Duration::from_millis(d)),
                    );
                }
            }
        };
    }

    fn on_exited(&mut self, ctx: &mut os::Ctx) {
        self.state = if self.removing {
            ServiceState::Removed
        } else if self.defn.auto_restart {
            let d = self.defn.restart_delay_ms.unwrap_or(2000);
            ServiceState::WaitingForStartTimeout(ctx.new_timeout(Duration::from_millis(d)))
        } else {
            ServiceState::Done
        };
    }

    fn first_wait_timeout(&self) -> Duration {
        Duration::from_millis(self.defn.shutdown_stage_timeout_ms.unwrap_or(2000))
    }

    fn next_wait_stage(&self, stg: WaitStage) -> (pr::Signal, WaitStage, Duration) {
        // TODO: more configurable stages?
        let d = self.defn.shutdown_stage_timeout_ms.unwrap_or(2000);
        match stg {
            WaitStage::Initial => (pr::Signal::Term, WaitStage::Soft, Duration::from_millis(d)),
            WaitStage::Soft | WaitStage::Hard => {
                (pr::Signal::Kill, WaitStage::Hard, Duration::from_millis(d))
            }
        }
    }
}

pub struct Supervisor {
    services: HashMap<String, Service>,
}

fn split_tracked_pid(track: pr::Pid, pids: Vec<PidState>) -> (Option<PidState>, Vec<PidState>) {
    let (mut trackeds, others): (_, Vec<_>) = pids
        .into_iter()
        .partition(|PidState { pid, state: _ }| *pid == track);
    debug_assert!(trackeds.len() <= 1, "pid {:?}: {:?}", track, trackeds);
    (trackeds.pop(), others)
}

fn try_reap(proc: &PidState) -> bool {
    if proc.state.is_terminated() {
        log::debug!("{:?} is {:?}, trying to reap", proc.pid, proc.state);
        os::try_reap(proc.pid).is_err()
    } else {
        log::trace!("{:?} is not terminated, not reaping, keeping", proc.pid);
        true
    }
}

fn check_group_shutdown(
    name: &str,
    grps: &mut HashMap<os::GroupId, Vec<PidState>>,
    grp: os::GroupId,
) -> bool {
    if let Some(members) = grps.remove(&grp) {
        // Attempting to abstract the FreeBSD implementation detail of a "group" not being
        // a specific object, but rather the first PID we started, that we keep around as a zombie
        // until the death of the whole group to 100% guarantee that no PID collision could possibly happen
        let mut to_reap = if let Some(tracked) = os::group_no_reap_pid(grp) {
            let (zmb, others) = split_tracked_pid(tracked, members);
            if let Some(PidState { pid, state }) = zmb {
                debug_assert_eq!(pid, grp);
                log::trace!(
                    "service {} has main pid {:?} in state {:?}",
                    name,
                    pid,
                    state
                );
            } else {
                warnic!(
                    "service {} has no main pid, we already reaped it somehow",
                    name
                );
            }
            others
        } else {
            members
        };
        log::trace!("trying to reap members of the group");
        to_reap.retain(try_reap);
        if to_reap.is_empty() {
            log::debug!(
                "service {} group is empty, cleaning group {:?} now",
                name,
                grp
            );
            if let Err(e) = os::clean_group(grp) {
                log::warn!("service {} failed to clean group: {:?}", name, e)
            }
            true
        } else {
            log::debug!("service {} group has {} procs left", name, to_reap.len());
            false
        }
    } else {
        // no processes running anymore
        log::debug!("service {} has nothing running, group has shut down", name);
        true
    }
}

impl Supervisor {
    pub fn new() -> Self {
        Supervisor {
            services: HashMap::with_capacity(8),
        }
    }

    pub fn add_service(&mut self, name: String, defn: def::Service) {
        if let Some(s) = self.services.get_mut(&name) {
            log::debug!("updated service {}", name);
            // TODO: optionally, mark for restarting if significant fields change
            s.defn = defn;
        } else {
            log::debug!("added service {}", name);
            self.services.insert(name, Service::new(defn));
        }
    }

    pub fn remove_service(&mut self, name: String) {
        if let Some(s) = self.services.get_mut(&name) {
            log::debug!("removing service {}", name);
            s.removing = true;
            // TODO: signal to shutdown
        }
    }

    pub fn on_process_groups_change(&mut self, ctx: &mut os::Ctx) {
        log::debug!("checking the state of process groups due to pid state change");
        let mut grps = os::process_groups().unwrap_or_else(|_| {
            warnic!("failed to get process group state");
            #[allow(unreachable_code)] // it's not unreachable in release mode!
            HashMap::new()
        });
        for (name, serv) in self.services.iter_mut() {
            serv.state.log(name);
            match serv.state {
                ServiceState::Fresh => serv.start(name, ctx),
                ServiceState::Running(svcpid, grp) => {
                    if let Some(members) = grps.remove(&grp) {
                        let (main, mut others) = split_tracked_pid(svcpid, members);
                        log::trace!("trying to reap other members of the group");
                        others.retain(try_reap);
                        if let Some(PidState { pid, state: pstate }) = main {
                            debug_assert_eq!(pid, svcpid);
                            log::debug!("service {} main pid is {:?}", name, pstate);
                            if pstate.is_terminated() {
                                if others.is_empty() {
                                    log::debug!(
                                        "service {} group is empty, reaping main process {:?} now",
                                        name,
                                        svcpid
                                    );
                                    if let Err(e) = os::try_reap(svcpid) {
                                        log::warn!(
                                            "service {} failed to reap main process: {:?}",
                                            name,
                                            e
                                        )
                                    }
                                    serv.on_exited(ctx);
                                } else {
                                    log::debug!(
                                        "service {} group has {} other procs, waiting now",
                                        name,
                                        others.len()
                                    );
                                    serv.state = ServiceState::WaitingForGroupShutdown(
                                        grp,
                                        WaitStage::Initial,
                                        ctx.new_timeout(serv.first_wait_timeout()),
                                    );
                                }
                            }
                        } else {
                            warnic!(
                                "service {} main proc {:?} was unexpectedly reaped already",
                                name,
                                svcpid
                            );
                        }
                    } else {
                        // no processes running anymore
                        log::warn!("service {} is not actually running", name);
                        serv.on_exited(ctx);
                    }
                }
                ServiceState::WaitingForGroupShutdown(grp, _, _) => {
                    if check_group_shutdown(name, &mut grps, grp) {
                        serv.on_exited(ctx);
                    }
                }
                _ => (),
            }
        }
        self.services
            .retain(|_, s| s.state != ServiceState::Removed);
    }

    pub fn on_timeout(&mut self, ctx: &mut os::Ctx, ticked: os::Timeout) {
        log::debug!("checking the state of process groups due to timer tick");
        let mut grps = os::process_groups().unwrap_or_else(|_| {
            warnic!("failed to get process group state");
            #[allow(unreachable_code)] // it's not unreachable in release mode!
            HashMap::new()
        });
        for (name, serv) in self.services.iter_mut() {
            serv.state.log(name);
            match serv.state {
                ServiceState::Fresh => serv.start(name, ctx),
                ServiceState::WaitingForGroupShutdown(grp, stg, timeout) if timeout == ticked => {
                    log::debug!("service {} shutdown timer ticked in stage {:?}", name, stg);
                    if check_group_shutdown(name, &mut grps, grp) {
                        serv.on_exited(ctx);
                        break;
                    }
                    let (sig, new_stg, timeout) = serv.next_wait_stage(stg);
                    log::trace!("sending {:?} to group {:?}", sig, grp);
                    let _ = os::signal_group(grp, sig);
                    log::trace!("sent {:?} to group {:?}, waiting again", sig, grp);
                    serv.state = ServiceState::WaitingForGroupShutdown(
                        grp,
                        new_stg,
                        ctx.new_timeout(timeout),
                    );
                }
                ServiceState::WaitingForStartTimeout(timeout) if timeout == ticked => {
                    log::debug!("service {} start timer ticked", name);
                    serv.start(name, ctx);
                }
                _ => (),
            }
        }
        self.services
            .retain(|_, s| s.state != ServiceState::Removed);
    }
}
