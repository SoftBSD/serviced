mod def;
mod supervisor;

#[cfg(target_os = "freebsd")]
mod freebsd;
#[cfg(target_os = "freebsd")]
use freebsd as os;

fn main() -> anyhow::Result<()> {
    os::main()
}
