use std::collections::HashMap;

// silly serde
fn return_true() -> bool {
    true
}

#[derive(serde::Deserialize)]
pub enum FdTarget {
    StdinStdout,
    Env(String),
    ListenFd(Option<String>),
}

#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum FdAssignment {
    Socket {
        #[serde(rename = "socket")]
        name: String,
        to: FdTarget,
        #[serde(default)]
        nonblocking: bool,
        // TODO: cap_rights?
    },
    File {
        #[serde(rename = "file")]
        path: String,
        to: FdTarget,
        // TODO: mode, cap_rights, ??
    },
    // TODO: tty stuff (with exclusive handling)
}

#[derive(serde::Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum SockType {
    Stream,
    Dgram,
    Seqpacket,
}

#[derive(serde::Deserialize)]
pub struct InetOpts {
    #[serde(rename = "type")]
    pub typ: SockType,
    #[serde(default)]
    pub reuse_port: bool,
}

#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum Listen {
    Inet4 {
        #[serde(rename = "inet4")]
        addr: String,
        #[serde(flatten)]
        opts: InetOpts,
    },
    Inet6 {
        #[serde(rename = "inet6")]
        addr: String,
        #[serde(flatten)]
        opts: InetOpts,
    },
    Unix {
        #[serde(rename = "unix")]
        path: String,
        #[serde(rename = "type")]
        typ: SockType,
        user: Option<String>,
        group: Option<String>,
        mode: Option<u8>,
        dir_mode: Option<u8>,
        #[serde(default)]
        symlinks: Vec<String>,
    },
}

#[derive(serde::Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum SpecialRole {
    Syslog,
    Devd,
    Dbus,
}

#[derive(serde::Deserialize)]
pub struct Socket {
    pub listen: Listen,
    #[serde(default)]
    pub accept: bool,
    pub role: Option<SpecialRole>,
    pub service: Option<String>,
}

#[derive(serde::Deserialize)]
pub struct Service {
    pub command: Vec<String>,
    pub chdir: Option<String>,
    pub user: Option<String>,
    pub group: Option<String>,
    #[serde(default)]
    pub supplementary_groups: Vec<String>,
    #[serde(default)]
    pub cap_mode: bool,
    #[serde(default)]
    pub env: HashMap<String, String>,
    #[serde(rename = "fd")]
    #[serde(default)]
    pub fds: Vec<FdAssignment>,
    pub max_start_failures: Option<u32>,
    #[serde(default = "return_true")]
    pub auto_restart: bool,
    pub restart_delay_ms: Option<u64>,
    pub failure_restart_delay_ms: Option<u64>,
    pub shutdown_stage_timeout_ms: Option<u64>,
    // TODO: pre/post cmds, umask, nice, cpuset, rlimits, coredump, (un)ignore sigpipe, no-new-priv,
    // dynamic users, pam name?, log opts for stderr/out, log extra fields, watchdog, kill behavior
}

#[derive(serde::Deserialize)]
pub struct File {
    #[serde(default)]
    #[serde(rename = "service")]
    pub services: HashMap<String, Service>,
    #[serde(default)]
    #[serde(rename = "socket")]
    pub sockets: HashMap<String, Socket>,
    // TODO: timers
    // fun possibility: shared memfds like sockets
}
