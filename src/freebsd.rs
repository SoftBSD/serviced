use crate::{
    def,
    supervisor::{PidState, ProcState, Supervisor},
};
use libc as c;
use rustix::{
    cstr,
    event::kqueue as kq,
    fd::{AsFd, BorrowedFd},
    io,
    process::Signal as Sig,
    process::{self as pr, PidInfo, PidInfoFlags},
};
use std::{
    collections::HashMap,
    ffi, fs, mem,
    os::unix::{prelude::OpenOptionsExt, process::CommandExt},
    ptr, time,
};

macro_rules! iovstr {
    ($str:expr) => {{
        let s = cstr!($str);
        c::iovec {
            iov_base: s.as_ptr() as *mut _,
            iov_len: s.to_bytes_with_nul().len(),
        }
    }};
}

const TIM_PERIODIC: isize = 69;
const TIM_TIMEOUT_BASE: isize = 420;

pub type GroupId = pr::Pid;

#[derive(PartialEq, Clone, Copy)]
pub struct Timeout(isize);

pub struct Ctx<'q> {
    kq: BorrowedFd<'q>,
    next_timeout_id: isize,
}

impl<'q> Ctx<'q> {
    pub fn new_timeout(&mut self, dur: time::Duration) -> Timeout {
        self.next_timeout_id += 1;
        let id = TIM_TIMEOUT_BASE + self.next_timeout_id % (isize::MAX - TIM_TIMEOUT_BASE);
        let subs = &[kq::Event::new(
            kq::EventFilter::Timer {
                ident: id,
                timer: Some(dur),
            },
            kq::EventFlags::ADD | kq::EventFlags::CLEAR | kq::EventFlags::ONESHOT,
            id,
        )];
        let mut evts = Vec::new();
        unsafe { kq::kevent(self.kq, subs, &mut evts, None) }.unwrap(); // XXX
        Timeout(id)
    }
}

pub fn process_groups() -> anyhow::Result<HashMap<GroupId, Vec<PidState>>> {
    let mut m = HashMap::<GroupId, Vec<PidState>>::with_capacity(32);
    for PidInfo {
        flags,
        pid,
        subtree,
    } in pr::get_reaper_pids(None)?
    {
        // let state = match pr::waitid(
        //     pr::WaitId::Pid(pid),
        //     pr::WaitidOptions::EXITED | pr::WaitidOptions::NOHANG | pr::WaitidOptions::NOWAIT,
        // ) {
        //     Ok(Some(e)) if e.killed() || e.dumped() => ProcState::Killed,
        //     Ok(Some(e)) if e.exited() => ProcState::Exited,
        //     Ok(Some(_)) => ProcState::UnknownEnd,
        //     Ok(None) => ProcState::Running,
        //     Err(io::Errno::CHILD) => ProcState::Running,
        //     Err(e) => return Err(e.into()),
        // };
        let state = if flags.contains(PidInfoFlags::STOPPED) {
            ProcState::Stopped
        } else if flags.contains(PidInfoFlags::EXITING) {
            ProcState::Exiting
        } else if flags.contains(PidInfoFlags::ZOMBIE) {
            ProcState::Zombie
        } else {
            ProcState::Running
        };
        m.entry(subtree)
            .and_modify(|g| g.push(PidState { pid, state }))
            .or_insert_with(|| vec![PidState { pid, state }]);
    }
    Ok(m)
}

pub fn signal_group(grp: GroupId, sig: pr::Signal) -> anyhow::Result<()> {
    let _ = pr::reaper_kill(None, sig, false, Some(grp))?;
    Ok(())
}

pub fn try_reap(pid: pr::Pid) -> anyhow::Result<bool> {
    let res = pr::waitid(
        pr::WaitId::Pid(pid),
        pr::WaitidOptions::EXITED, // | pr::WaitidOptions::NOHANG,
    )?;
    log::debug!(
        "reaped {:?}:{:?}",
        pid,
        res.map(|x| x.killed() || x.dumped())
    );
    Ok(res.is_some()) // XXX: reasons to check returned status?
}

pub use try_reap as clean_group;

pub fn group_no_reap_pid(grp: GroupId) -> Option<pr::Pid> {
    Some(grp)
}

// TODO: obviously, do better
pub fn launch(cmd: Vec<String>) -> anyhow::Result<(pr::Pid, GroupId)> {
    let mut cmd: std::collections::VecDeque<ffi::OsString> =
        cmd.into_iter().map(|x| x.into()).collect();
    let pid = unsafe {
        std::process::Command::new(cmd.pop_front().unwrap())
            .args(cmd)
            .pre_exec(|| {
                pr::setsid()?;
                Ok(())
            })
            .spawn()?
            .id()
    };
    let pid = pr::Pid::from_raw(pid as _).unwrap();
    Ok((pid, pid))
}

#[derive(Clone, Copy, PartialEq)]
enum SigAct {
    Reap,
    Reload {
        auto: bool,
    },
    Shutdown(c::c_int),
    /// Stop spawning login sessions (until a `Reload` comes)
    EnterCatatonia,
    Reroot,
    Ignore,
}

const SIGTAB: &[(Sig, SigAct)] = &[
    (Sig::Child, SigAct::Reap),
    (Sig::Hup, SigAct::Reload { auto: false }),
    (Sig::Term, SigAct::Shutdown(0)), // We don't have single user mode
    (Sig::Int, SigAct::Shutdown(0)),
    (Sig::Winch, SigAct::Shutdown(c::RB_POWERCYCLE)),
    (Sig::Usr1, SigAct::Shutdown(c::RB_HALT)),
    (Sig::Usr2, SigAct::Shutdown(c::RB_HALT | c::RB_POWEROFF)),
    (Sig::Tstp, SigAct::EnterCatatonia),
    (Sig::Emt, SigAct::Reroot),
    (Sig::Ttin, SigAct::Ignore),
    (Sig::Ttou, SigAct::Ignore),
];

// Gotta go fast
const fn signal_lut() -> [SigAct; Sig::Librt as usize] {
    let mut tab = [SigAct::Ignore; Sig::Librt as usize];
    let mut i = 0;
    while i < SIGTAB.len() {
        tab[SIGTAB[i].0 as usize] = SIGTAB[i].1;
        i += 1;
    }
    tab
}

const SIGLUT: [SigAct; Sig::Librt as usize] = signal_lut();

// TODO: add to rustix
fn get_kenv(key: &ffi::CStr) -> anyhow::Result<ffi::CString> {
    let mut buf = [0u8; c::KENV_MVALLEN as usize + 1];
    let r = unsafe {
        c::kenv(
            c::KENV_GET,
            key.as_ptr(),
            buf.as_mut_ptr() as *mut _,
            buf.len() as _,
        )
    };
    if r == -1 {
        Err(anyhow::anyhow!("kenv"))
    } else {
        Ok(ffi::CStr::from_bytes_with_nul(&(buf.as_slice())[..r as usize])?.to_owned())
    }
}

#[derive(Clone, Copy)]
#[repr(u8)]
enum Trace {
    Boot,
    Run,
    Shut,
}

fn boottrace(kind: Trace, str: &str) {
    let name = match kind {
        Trace::Boot => cstr!("kern.boottrace.boottrace"),
        Trace::Run => cstr!("kern.boottrace.runtrace"),
        Trace::Shut => cstr!("kern.boottrace.shuttrace"),
    };
    let msg = ffi::CString::new(if str.len() > 64 { &str[..64] } else { str }).unwrap();
    let _ = unsafe {
        c::sysctlbyname(
            name.as_ptr(),
            ptr::null_mut(),
            ptr::null_mut(),
            msg.as_ptr() as *const _,
            msg.as_bytes().len(),
        )
    };
}

unsafe fn do_reboot(howto: i32) -> ! {
    c::sync();
    log::info!("serviced says bye :3");
    if c::reboot(howto) == -1 {
        log::error!("reboot({}) failed", howto);
        c::_exit(1);
    }
    log::error!("reboot({}) exited", howto);
    c::_exit(0);
}

pub fn main() -> anyhow::Result<()> {
    let we_are_init = pr::getpid() == pr::Pid::INIT;
    if we_are_init {
        boottrace(Trace::Boot, "starting up");

        // TODO: if doesn't exist already
        let devfs_args = &mut [
            iovstr!("fstype"),
            iovstr!("devfs"),
            iovstr!("fspath"),
            iovstr!("/dev"),
        ];
        unsafe { c::nmount(devfs_args.as_mut_ptr(), devfs_args.len() as _, 0) };

        let console = Box::new(
            fs::OpenOptions::new()
                .write(true)
                .custom_flags(c::O_NONBLOCK)
                .open("/dev/console")?,
        );
        use std::io::Write;
        env_logger::Builder::new()
            .target(env_logger::Target::Pipe(console))
            .filter_level(log::LevelFilter::Debug) // TODO
            .format(|buf, record| writeln!(buf, "serviced: {}\r", record.args()))
            .init();
        log::info!("serviced says hello :3");

        pr::setsid()?;
        unsafe { c::setlogin(cstr!("root").as_ptr()) };
    } else {
        env_logger::init();
        pr::set_reaper_status(true)?;
    }

    for (s, _) in SIGTAB {
        if *s == Sig::Child {
            // setting SIG_IGN on SIGCHLD will set PS_NOCLDWAIT so it won't get on the kqueue
            continue;
        }
        let act = c::sigaction {
            sa_sigaction: c::SIG_IGN,
            sa_flags: 0,
            sa_mask: unsafe {
                let mut mask_none = mem::MaybeUninit::<c::sigset_t>::uninit();
                c::sigemptyset(mask_none.as_mut_ptr());
                mask_none.assume_init()
            },
        };
        unsafe { c::sigaction(*s as _, &act, ptr::null_mut()) };
    }

    let subs = SIGTAB
        .iter()
        .map(|(s, _)| {
            kq::Event::new(
                kq::EventFilter::Signal {
                    signal: s.clone(),
                    times: 0,
                },
                kq::EventFlags::ADD,
                0,
            )
        })
        .chain([
            // SIGCHLD shouldn't be unreliable but still let's check up on processes periodically
            kq::Event::new(
                kq::EventFilter::Timer {
                    ident: TIM_PERIODIC,
                    timer: Some(time::Duration::from_secs(60)),
                },
                kq::EventFlags::ADD,
                0,
            ),
        ])
        .collect::<Vec<_>>();

    let kq = kq::kqueue()?;
    io::fcntl_setfd(&kq, io::FdFlags::CLOEXEC)?;
    let mut evts = Vec::new();
    unsafe { kq::kevent(&kq, &subs, &mut evts, None) }?;

    let mut ctx = Ctx {
        kq: kq.as_fd(),
        next_timeout_id: 0,
    };

    let mut sv = Supervisor::new();

    let mut from_kenv = Vec::new();
    if we_are_init {
        from_kenv.push(get_kenv(cstr!("serviced_paths"))?.to_str()?.to_owned());
    }
    for path in std::env::args().skip(1).chain(from_kenv) {
        let conf: def::File = toml::de::from_str(&fs::read_to_string(path)?)?;
        for (name, defn) in conf.services {
            sv.add_service(name, defn);
        }
    }

    if we_are_init {
        boottrace(Trace::Run, "starting services");
    }

    // Initial pulse to start fresh services
    sv.on_process_groups_change(&mut ctx);

    let mut evts = Vec::with_capacity(8);
    loop {
        match unsafe { kq::kevent(&kq, &[], &mut evts, None) } {
            Err(io::Errno::INTR) => Ok(0),
            x => x,
        }?;
        while let Some(ev) = evts.pop() {
            match ev.filter() {
                kq::EventFilter::Signal { signal, times: _ } => match SIGLUT[signal as usize] {
                    SigAct::Reap => sv.on_process_groups_change(&mut ctx),
                    SigAct::Reload { auto: _ } => log::warn!("TODO: reload not supported yet"),
                    SigAct::Shutdown(howto) => {
                        if we_are_init {
                            boottrace(Trace::Boot, "shutting down");
                        }
                        // TODO: shut services down softly first
                        if we_are_init {
                            unsafe { do_reboot(howto) }
                        }
                        std::process::exit(0);
                    }
                    SigAct::EnterCatatonia => log::error!("TODO: catatonia not supported yet"),
                    SigAct::Reroot => log::error!("TODO: reroot not supported yet"),
                    SigAct::Ignore => (),
                },
                kq::EventFilter::Timer {
                    ident: TIM_PERIODIC,
                    timer: _,
                } => {
                    sv.on_process_groups_change(&mut ctx);
                }
                kq::EventFilter::Timer { ident, timer: _ } if ident >= TIM_TIMEOUT_BASE => {
                    sv.on_timeout(&mut ctx, Timeout(ident));
                }
                _ => (),
            }
        }
    }
}
