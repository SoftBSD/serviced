# serviced: a simple but strong system service supervisor

serviced is a simple process supervisor inspired by the best parts of systemd, written in Rust for FreeBSD based systems.

Goals:

- target embedded-ish immutable-ish systems most of all
- be usable both as pid 1 and as a user/project-local thing
- leverage [socket activation](https://0pointer.de/blog/projects/socket-activation.html) for everything
- leverage OS facilities like FreeBSD's reaper API for inescapable tracking of process groups
  - but be portable in theory :) (abstract OS specifics at least somewhat in the code)
- support device events (subsume devd entirely?)
- support redirecting all logging to a syslog daemon like [squealogd](https://codeberg.org/SoftBSD/squealog)
- support arbitrary fd storage (`sd_notify` API)
- support [watchdogs](https://0pointer.de/blog/projects/watchdog.html) (`sd_notify` API and the system part too)
- provide [dynamic users](https://0pointer.net/blog/dynamic-users-with-systemd.html) (with varlink or not?)
- manage [gettys](https://0pointer.de/blog/projects/serial-console.html) in a smart way
- integrate with D-Bus activation
- use a reasonable standardized config syntax (i.e. TOML)
- do not enforce any particular directory layouts for config
  - be able to just run the whole system from one file, but support directories for modularity too
- just live reload the config by default, without any `daemon-reload` nonsense

Non-goals:

- targeting traditionally system-administrated boxes
- compatibility with legacy init systems
- compatibility with all possible weird things legacy daemons do
- introducing [new logging APIs](https://docs.google.com/document/u/0/d/1IC9yOXj7j6cdLLxWEBAGRL6wl97tFxgjLUEHIX3MSTs/pub), RFC 5424 syslog is good enough
- having the daemon itself listen on D-Bus directly

Status: only basic supervision is implemented so far, hopefully mostly correctly?

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).
